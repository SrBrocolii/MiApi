const express = require('express');
const morgan = require('morgan');
const app = express ();

/////////////////////////////////////////
//settings
app.set('appName','Mi primer server');

/////////////////////////////////////////
//Middlewares
app.use(morgan('combined'));

/////////////////////////////////////////
//RUTAS

const routes= require('./routes');
app.use(routes);
const routesApi = require('./routes-api');
app.use('/api',routesApi);
app.get('*',(req, res)=>{
  res.end('Archivo no encontrado');
});

/////////////////////////////////////////////

app.listen(3000, function(){
  console.log('servidor funcionando!')
  console.log('Nombre de la app:  ' , app.get('appName'));
});
